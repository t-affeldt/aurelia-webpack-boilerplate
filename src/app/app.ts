import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';

export class App {
	router: Router;

	configureRouter(config: RouterConfiguration, router: Router) {
		config.title = 'Aurelia';
		config.map([
			{
				route: '',
				name: 'home',
				moduleId: PLATFORM.moduleName('../home/home')
			}
		]);
		this.router = router;
	}
}