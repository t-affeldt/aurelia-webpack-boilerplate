const  { AureliaPlugin } = require('aurelia-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

	entry: {
		main: 'aurelia-bootstrapper'
	},

	output: {
		path: path.resolve(__dirname, 'dist'),
		publicPath: '',
		filename: '[name].js',
		chunkFilename: '[name].js'
	},

	target: 'web',

	module: {
		loaders: [
			{
				test: /\.js$/i,
				exclude: /node_modules/,
				loader: 'source-map-loader'
			},
			{
				test: /\.ts$/i,
				exclude: /node_modules/,
				loader: 'awesome-typescript-loader'
			},
			{
				test: /\.html?$/i,
				exclude: path.resolve('src/index.html'),
				loader: 'html-loader'
			},
			{
				test: /\.s(a|c)ss$/i,
				loaders: [
					'css-loader',
					'sass-loader'
				]
			},
			{
				test: /\.css$/i,
				loader: 'css-loader'
			},
			{
				test: /\.json$/i,
				loader: 'json-loader'
			}
		]
	},

	resolve: {
		extensions: ['.ts', '.js'],
		modules: ['src', 'node_modules'].map(x => path.resolve(x))
	},

	plugins: [
		new AureliaPlugin(),
		new HtmlWebpackPlugin({
			template: 'index.html',
			chunksSortMode: 'dependency'
		}),
		new webpack.ProvidePlugin({
			'Promise': 'bluebird'
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'main'
		}),
		new webpack.optimize.OccurrenceOrderPlugin()
	],

	devServer: {
		port: 3000
	}

}